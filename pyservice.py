import io
import math
import json
import time
import sqlite3
import http.server

#########################################################
# This is sloppy as shit but it's free so deal with it =)
#########################################################

# The speed at which we switch from green to yellow.
speed_warn = 35
# The speed at which we switch from yellow to red.
speed_bad = 40

# Basic rounding function.
def round_up(n, decimals=0):
    multiplier = 10 ** decimals
    return math.ceil(n * multiplier) / multiplier

# Calculate our speed.
def calc_speed(val):
    speed = (val / 31.37)
    return round_up(speed, 2)

# Calculate our direction.
def calc_direction(val):
    if val == 1:
        direction = "North"
    else:
        direction = "South"
    return direction

# Calculate our time.
def calc_time(val):
    return time.strftime("%m/%d/%Y %I:%M:%S %p", time.localtime(val))

# Build tile box UI.
def ui_tile(title, direction, speed, timestamp):
    if isinstance(direction, int):
      this_direction = str(calc_direction(direction))
    else:
      this_direction = direction
    if isinstance(timestamp, float):
      this_time = str(calc_time(timestamp))
    else:
      this_time = timestamp
    this_class = "alert-success"
    if calc_speed(speed) > speed_warn:
        this_class = "alert-warning"
    if calc_speed(speed) > speed_bad:
        this_class = "alert-danger"
    data = "<div class=\"col-12 col-md-6 col-lg-4 col-xl-3\">"
    data = data + "<h5>" + title + "</h5>"
    data = data + "<div class=\"alert " + this_class + "\" style=\"text-align:center;\">"
    data = data + "<span style=\"font-weight:light;\">" + str(this_direction) + "</span><br>"
    data = data + "<span style=\"font-size:3em;font-weight:bold;\">" + str(calc_speed(speed)) + "</span><br>"
    data = data + "<span style=\"font-size:0.9em;font-weight:light;position:relative;bottom:1em;\">mph</span><br>"
    data = data + "<span style=\"font-weight:light;\">" + str(this_time) + "</span>"
    data = data + "</div>"
    data = data + "</div>"
    return data

# Build the table UI.
def ui_table(rows):
    data = "<div class=\"row\" style=\"margin-bottom:2em;\">"
    data = data + "<div class=\"col-12\">"
    data = data + "<h5>Last 24hr</h5>"
    data = data + "<table class=\"table table-striped\">"
    data = data + "<thead>"
    data = data + "<tr>"
    data = data + "<th scope=\"col\">Timestamp</th>"
    data = data + "<th scope=\"col\">Speed</th>"
    data = data + "<th scope=\"col\">Direction</th>"
    data = data + "</tr>"
    data = data + "</thead>"
    data = data + "<tbody>"
    for row in rows:
        data = data + "<tr>"
        data = data + "<th scope=\"row\">" + str(calc_time(row[0])) + "</th>"
        data = data + "<td>" + str(calc_speed(row[1])) + " mph</td>"
        data = data + "<td>" + str(calc_direction(row[2])) + "</td>"
        data = data + "</tr>"
    data = data + "</tbody>"
    data = data + "</table>"
    data = data + "</div>"
    return data

# Build a page to display our data.
def render_page(val):

    # Define things which need definition.
    yesterday = (time.time() - 86400)

    # Connect to the database and gather up some data.
    conn = sqlite3.connect('speed.db')
    cursor = conn.cursor()
    cursor.execute("SELECT * FROM traffic ORDER BY timestamp DESC LIMIT 1")
    last_record = cursor.fetchone()
    cursor.execute("SELECT * FROM traffic WHERE timestamp > ? ORDER BY timestamp DESC", (yesterday,))
    last_24 = cursor.fetchall()
    cursor.execute("SELECT AVG(speed) FROM traffic WHERE timestamp > ?", (yesterday,))
    speed_avg = cursor.fetchone()
    cursor.execute("SELECT * FROM traffic WHERE timestamp > ? ORDER BY speed DESC", (yesterday,))
    speed_record = cursor.fetchone()
    conn.commit()
    conn.close()

    # Load the template header.
    with open('templates/bootstrap_header.html', 'r') as file:
        page_data = file.read().replace('\n', '')
    
    # Start with the tiles first.
    page_data = page_data + "<div class=\"container\">"
    page_data = page_data + "<div class=\"row\" style=\"margin-bottom:2em;\">"
    # Last recorded speed.
    page_data = page_data + ui_tile("Last Recorded Speed", last_record[2], last_record[1], last_record[0])
    # 24hr highest speed.
    page_data = page_data + ui_tile("24hr Highest Speed", speed_record[2], speed_record[1], speed_record[0])
    # 24hr average speed.
    page_data = page_data + ui_tile("24hr Average Speed", "--", speed_avg[0], "--")
    page_data = page_data + "</div>"
    # Add in a table just for fun.
    page_data = page_data + ui_table(last_24)
    page_data = page_data + "</div>"

    # Load the template footer.
    with open('templates/bootstrap_footer.html', 'r') as file:
        page_data = page_data + file.read().replace('\n', '')

    return page_data

# Handle incoming requests.
class handle_requests(http.server.BaseHTTPRequestHandler):
    
    # Handle incoming GET requests.
    def do_GET(self):
        self.send_response(200)
        self.send_header('Content-type','text/html')
        self.end_headers()
        self.wfile.write(str.encode(render_page(1)))

    # Handle incoming POST requests.
    def do_POST(self):
        # Read in the payload data.
        content_length = int(self.headers['Content-Length'])
        payload = self.rfile.read(content_length)
        
        # Parse the JSON payload into an object.
        data_obj = json.loads(payload)

        # Set some variables to keep it neat.
        ts = time.time()
        speed = data_obj['speed']
        direction = data_obj['direction']

        # Connect to the database and write data.
        conn = sqlite3.connect('speed.db')
        cursor = conn.cursor()
        cursor.execute("INSERT INTO traffic VALUES (?, ?, ?)", (ts, speed, direction))
        conn.commit()
        conn.close()

        # Send a response.
        self.send_response(200)
        self.end_headers()
        response = io.BytesIO()
        response.write(b'Success')
        self.wfile.write(response.getvalue())

# Run the HTTP server forever on the specified port.
service = http.server.HTTPServer(('localhost', 8888), handle_requests)
service.serve_forever()

