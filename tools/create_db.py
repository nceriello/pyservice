import sqlite3

conn = sqlite3.connect('speed.db')
cursor = conn.cursor()
cursor.execute('CREATE TABLE traffic(timestamp integer, speed integer, direction integer)')
conn.commit()
conn.close()
